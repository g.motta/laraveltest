<?php

namespace App\Http\Controllers;

use Illuminate\Http\Response;
use Illuminate\Http\Request;
use App\Models\Product;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            $products = Product::all();

            if ($request->is('api/*')) {

                return response($products, 200);
            } else {
                return view('showProduct', ['products' => $products]);
            }
        } catch (\Throwable $e) {
            return response($e, 500);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        try {

            $validatedData = $request->validate([
                'name' => 'required|min:5',
                'description' => 'required|max:255',
            ]);



            $product = new Product();
            $product->name = $request->name;
            $product->description = $request->description;

            //if called from api 
            if ($request->is('api/*')) {
                if ($product->save()) {

                    return response($product, 200);
                } else {
                    return response("Ocurrio un error cuando se intentaba guardar el producto", 500);
                }
            } else {

                if ($product->save()) {
                    return back()->with('successMessage', "Pructo creado de manera correcta"); //view('showProduct', ['successMessage' => "Pructo creado de manera correcta"]);
                } else {
                    return back()->with('errorMessage', "Ocurrio un error cuando se intentaba guardar el producto");
                }
            }
        } catch (\Throwable $e) {
            return response($e, 500);
        }
    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $product = Product::find($id);

            return response($product, 200);
        } catch (\Throwable $e) {
            return response($e, 500);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        try {

            $product = Product::find($request->id);
            $product->name = $request->name;
            $product->description = $request->description;

            if ($product->save()) {
                return response("producto actualizado de manera correcta.", 200);
            } else {
                return response("Ocurrio un error cuando se intentaba actualizar el producto", 500);
            }
        } catch (\Throwable $e) {
            return response($e, 500);
        }
    }



    /**
     * Revemove productos
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id, Request $request)
    {
        try {

            $product = Product::find($id);

            if ($request->is('api/*')) {

                if ($product->delete()) {
                    return response("producto eliminado de manera correcta.", 200);
                } else {
                    return response("Ocurrio un error cuando se intentaba eliminar el producto", 500);
                }
            } else {

                if ($product->delete()) {
                    return back()->with('successMessage', "Pructo eliminado de manera correcta"); //view('showProduct', ['successMessage' => "Pructo creado de manera correcta"]);
                } else {
                    return back()->with('errorMessage', "Ocurrio un error cuando se intentaba eliminar el producto");
                }
            }
        } catch (\Throwable $e) {
            return response($e, 500);
        }
    }

    public function deleteProduct($id, Request $request)
    {
        try {

            $product = Product::find($id);

            if ($product->delete()) {
                return back()->with('successMessage', "Pructo eliminado de manera correcta"); //view('showProduct', ['successMessage' => "Pructo creado de manera correcta"]);
            } else {
                return back()->with('errorMessage', "Ocurrio un error cuando se intentaba eliminar el producto");
            }

            
        } catch (\Throwable $e) {
            return response($e, 500);
        }
    }
}
