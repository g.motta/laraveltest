<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ProductTest extends TestCase
{
    /**
     @test
     */
    public function get_a_product()
    {
        $response = $this->get('/api/products/');

        $response->assertSee(1);
        $response->assertStatus(200);
    }

    /**
     @test
     */
    public function create_a_product()
    {
   
        $response = $this->postJson('/api/products', ['name' => 'Peritas', 'description' =>  'Linda Perita']);

        $response->assertSuccessful();
    }
}
