<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProductController;


Route::get('/', function () {
    return view('showProduct');
});


Route::get('/getProducts',[ProductController::class, 'index']);

Route::get('products', [ProductController::class, 'index']);

Route::post('products', [ProductController::class, 'create']);

Route::get('products/delete/{id}', [ProductController::class, 'deleteProduct']);