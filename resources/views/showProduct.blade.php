@extends('welcome')

@section('title', 'Productos')

@section('main')
    @if (session('errorMessage'))
        <div class="alert alert-secondary" role="alert">
            {{ session('errorMessage') }}
        </div>
    @endif
    @if (session('successMessage'))
        <div class="alert alert-success" role="alert">
            {{ session('successMessage') }}
        </div>
    @endif
    <table class="table table-sm mt-5 pt-5 ">
        <thead>
            <tr>
                <th scope="col">#ID</th>
                <th scope="col">Nombre</th>
                <th scope="col">Descripción</th>
                <th scope="col">Fecha de creación</th>
                <th scope="col">Herramientas</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($products as $p)
                <tr>
                    <th scope="row">{{ $p->id }}</th>
                    <td>{{ $p->name }}</td>
                    <td>{{ $p->description }}</td>
                    <td>{{ $p->created_at }}</td>
                    <td>
                        <a type="button" href="/products/delete/{{$p->id}}}"><i class=" fas fa-trash-alt"></i> Eliminar</a>
                    </td>
                </tr>
            @endforeach

        </tbody>
    </table>
@endsection
